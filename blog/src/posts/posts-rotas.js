const postsControlador = require('./posts-controlador');
const middleware = require('../usuarios/middleware-usuario');

module.exports = app => {
  app
    .route('/post')
    .get(postsControlador.lista)
    .post(middleware.bearer,
          postsControlador.adiciona);
};
