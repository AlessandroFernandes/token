const passport = require('passport')

module.exports = {
    local: (req, res, next) => {
        passport.authenticate('local', { session: false},
        (erro, usuario, info) => {

            if(erro && erro.name === "InvalidArgumentError")
            {
                return res.status(401).json({ erro : erro.message })
            }

            if(erro) 
            {
                return res.status(500).json({ erro : erro.message })
            }

            if(!usuario) 
            {
               // console.log(req)
                return res.status(401).json()
            }

            req.user = usuario;
            return next();

        })(req, res, next)
    },

    bearer: (req, resp, next) => {
        passport.authenticate('bearer', { session: false},
        (error, usuario, info) => {
            if(error && error.message === "JsonWebTokenError")
            {
                return resp.status(401).json({ error: error.message })
            }

            if(error && error.message === "JsonExperedError")
            {
                return resp.status(401).json({ error: error.message, expered: error.experedAt })
            }

            if(error)
            {
                return resp.status(500).json({ error: error.message })
            }

            if(!usuario)
            {
                return resp.status(500).json()
            }
            
            req.token = info.token;
            req.user = usuario;
            return next();

        })(req, resp, next)
    }
}