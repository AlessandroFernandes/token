const Usuario = require('./usuarios-modelo');
const { InvalidArgumentError, InternalServerError } = require('../erros');
const jwt = require('jsonwebtoken');
const blacklist = require('../redis/blacklist');

function criarToken(usuario) {
  const payLoad =  {
    id: usuario.id
  }
  return jwt.sign(payLoad, process.env.SALT, { expiresIn: '15m' });

}

module.exports = {
  adiciona: async (req, res) => {
    const { nome, email, senha } = req.body;

    try {
      const usuario = new Usuario({
        nome,
        email,
      });
      await usuario.adicionarSenhaHash(senha)
      await usuario.adiciona();

      res.status(201).json();
    } catch (erro) {
      if (erro instanceof InvalidArgumentError) {
        res.status(422).json({ erro: erro.message });
      } else if (erro instanceof InternalServerError) {
        res.status(500).json({ erro: erro.message });
      } else {
        res.status(500).json({ erro: erro.message });
      }
    }
  },

  lista: async (req, res) => {
    const usuarios = await Usuario.lista();
    res.json(usuarios);
  },

  login: (req, res) => {
    const token = criarToken(req.user);
    res.set('Authorization', token);
    res.status(200).send();
  },

  logout: async (req, res) => {
    try
    {
      const token = req.token;
      await blacklist.adicionar(token);
      res.status(204).json();
    }
    catch(error)
    {
      res.status(500).json({token: "Token expirado via logout"})
    }
  },

  deleta: async (req, res) => {
    const usuario = await Usuario.buscaPorId(req.params.id);
    try {
      await usuario.deleta();
      res.status(200).send();
    } catch (erro) {
      res.status(500).json({ erro: erro });
    }
  }
};
