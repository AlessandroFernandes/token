
const usuariosControlador = require('./usuarios-controlador');
const middleware = require('./middleware-usuario')
const blacklist = require('../redis/blacklist');

module.exports = app => {

  app
    .route('/usuario/login')
    .post(middleware.local,
          usuariosControlador.login);

  app
    .route('/usuario/logout')
    .get(middleware.bearer,
         usuariosControlador.logout)

  app
    .route('/usuario')
    .post(usuariosControlador.adiciona)
    .get(usuariosControlador.lista)
    

  app.route('/usuario/:id')
  .delete(middleware.bearer, 
          usuariosControlador.deleta);
};
