const redis = require('./redis.config');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const promisify = require('util').promisify;

criarHashToken = token => {
    return crypto.createHash('sha256').update(token).digest('hex');
}

const setRedis = promisify(redis.set).bind(redis);
const existsRedis = promisify(redis.exists).bind(redis);

module.exports = {
   
    adicionar: async token => {
        const jsonExp = jwt.decode(token).exp;
        const hashToken = criarHashToken(token);
        await setRedis(hashToken, '');
        redis.expireat(hashToken, jsonExp);
    },
    contem: async token => {
        const hashToken = criarHashToken(token);
        const contem = await existsRedis(hashToken);
        return contem === 1;
    }
}