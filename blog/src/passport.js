const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const BearerStrategy = require('passport-http-bearer').Strategy
const jwt = require('jsonwebtoken');

const bcrypt = require('bcrypt')
const { InvalidArgumentError } = require('./erros')
const usuarioModelo = require('./usuarios/usuarios-modelo')
const { TokenExpiredError } = require('jsonwebtoken');
const Usuario = require('./usuarios/usuarios-modelo');
const blacklist = require('./redis/blacklist');

function validaUsuario(usuario) {
    if(!usuario) {
        throw new InvalidArgumentError('Usuário não encontrado')
    }
};

async function validaSenha(senha, senhaHash) {
    const senhaCompare =  await bcrypt.compare(senha, senhaHash)
    if(!senhaCompare) {
        throw new InvalidArgumentError('Senha inválida')
    }
};

async function contemBlacklist(token) {
    const tokenBlacklist = await blacklist.contem(token);
    if(tokenBlacklist)
    {
        throw new jwt.JsonWebTokenError("Token expirado via logout!");
    }
}

passport.use(
    new LocalStrategy({
        usernameField: 'email',
        passwordField: 'senha',
        session: false
    }, async (email, senha, done) => {
        try 
        {
            const usuario = await usuarioModelo.buscaPorEmail(email);
            validaUsuario(usuario);
            await validaSenha(senha, usuario.senhaHash);
            done(null, usuario);
        }
        catch(e) 
        {
            done(e)
        }
    })
)

passport.use(
    new BearerStrategy(
        async (token, done) => {
            try 
            {
                await contemBlacklist(token);
                const payload = jwt.verify(token, process.env.SALT);
                const user = Usuario.buscaPorId(payload.id);
                done(null, user, {token: token});
            }
            catch(error)
            {
                done(error)
            }
           
    })
)