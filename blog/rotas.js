const posts = require('./src/posts');
const usuarios = require('./src/usuarios');

const passport = require('./src/passport');

module.exports = app => {
  app.get('/', (req, res) => usuarios.controlador.login());
  
  posts.rotas(app);
  usuarios.rotas(app);
};